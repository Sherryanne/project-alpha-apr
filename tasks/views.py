from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create.html", {"form": form})


@login_required
def tasks_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/list.html", {"tasks": tasks})
